<div id="shwcf" class="sh-form">
  <div class="tabs">
      <?php for($x = 1; $x <= 2; $x++ ): ?>
        <div class="tab <?php $selected = ($x == 1) ? 'selected': ''; echo $selected; ?>" data-id="<?php echo $x;?>">
          <span>Step <?php echo $x;?></span>
        </div>
      <?php endfor; ?>
      <div class="tab last">
        <span><a href="#" class="cancel">Cancel website creation</a></span>
      </div>
  </div>
  <div class="content">
    <form>
      <div id="s1" class="step active">
        <div class="headline">
          <span class="title"><b>Step 1</b> - Setup your website details.</span>
        </div>
        <div class="group">
          <div class="field">
            <label>Select website version</label>
            <div class="select">
              <span class="dropdown-el">
                <input type="radio" name="type" value="wordpress" checked="checked" id="wordpress"><label for="wordpress">Wordpress</label>
                <input type="radio" name="type" value="woocommerce" id="woocommerce"><label for="woocommerce">Wordpress with Woocommerce</label>
                <input type="radio" name="type" value="multisite" id="multisite"><label for="multisite">Wordpress Multisite</label>
              </span>
            </div>
          </div>
          <div class="field">
            <label>Set website name</label>
            <input type="text" name="name" id="websiteName">
            <span class="alert name">You need to provide a name of your website.</span>
          </div>
        </div>
      </div>
      <div id="s2" class="step">
        <div class="headline">
          <span class="title"><b>Step 2</b> - Select starter theme.</span>
        </div>
        <div class="group">
          <p>You can select a starter theme from the selection below or start from scratch. </p>
          <div class="field themes">
            <?php foreach($args['themes'] as $key => $theme): ?>
            <div class="theme option-<?php echo $key; ?>">
              <input type="radio" name="theme" value="<?php echo $key; ?>">
              <label class="preview" style="background: url('<?php echo wp_get_attachment_image_src($theme[0], 'full')[0]; ?>')"></label>
              <a href="<?php echo $theme[2]; ?>" target="_blank" class="name"><?php echo $theme[1]; ?>
                <span>Click here to preview</span>
              </a>
            </div>
          <?php endforeach; ?>
          </div>
        </div>
      </div>
      <div id="s3" class="step">
      </div>
      <div class="actions step-1 active">
        <button id="next" type="button" class="btn sh-primary-btn"> NEXT STEP </button>
      </div>

      <div class="actions step-2">
        <button id="back" type="button" class="btn sh-tertiary-btn"> <i class="fas fa-long-arrow-alt-left"></i> BACK TO PREVIOUS STEP</button>
        <button id="fromScratch" type="button" class="btn sh-secondary-btn"> START FROM SCRATCH </button>
        <button id="create" type="button" class="btn sh-primary-btn"> CREATE WEBSITE </button>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  var loaderFile = '<?php echo wp_get_attachment_image_src($args['loader'], 'full')[0]; ?>';
  var assetPath = '<?php echo plugin_dir_url( dirname( __FILE__ ) ); ?>assets';
</script>
