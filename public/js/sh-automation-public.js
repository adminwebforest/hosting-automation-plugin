(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 /**
 	 * Website Form Tabs Function
 	 * Trigger tab content
	 */

	 console.log('Init');

	 let requestActive = false;

	 $(document).on('click', '#shwcf .tabs .tab', function(){
		 const id = $(this).data('id');
		 console.log(id);
		 if(id !== undefined && !requestActive) {
			 $('#shwcf .tabs .tab').removeClass('selected');
			 $(this).addClass('selected');
			 // Display content tab
			 $('#shwcf .content .step').removeClass('active');
			 $('#s' + id).addClass('active');
			 toggleFormButtons(id);
		 }
	 });

	 /**
		* Custom Select for SH Function
		* Trigger tab content
	*/

	 $(document).on('click', '#shwcf form .group .field .select .dropdown-el', function(e){
	  e.preventDefault();
	  e.stopPropagation();
	  $(this).toggleClass('expanded');
	  $('#'+$(e.target).attr('for')).prop('checked',true);
	});

	$(document).click(function() {
	  $('#shwcf form .group .field .select .dropdown-el').removeClass('expanded');
	});

	$(document).on('click', '#shwcf form #next', function(e) {
		if($('#websiteName').val() !== '') {
			e.preventDefault();
		  e.stopPropagation();
			$('.alert').removeClass('active');
			$('#shwcf .tabs .tab').removeClass('selected');
			$('#shwcf .tabs .tab:nth-child(2)').addClass('selected');
			// Display content tab
			$('#shwcf .content .step').removeClass('active');
			$('#s2').addClass('active');
			toggleFormButtons(2);
		} else {
			$('.alert.name').addClass('active');
		}
	});

	$(document).on('click', '#shwcf form #back', function(e) {
		e.preventDefault();
	  e.stopPropagation();
		toggleFormButtons(1);
		$('#shwcf .tabs .tab').removeClass('selected');
		$('#shwcf .tabs .tab:nth-child(1)').addClass('selected');
		// Display content tab
		$('#shwcf .content .step').removeClass('active');
		$('#s1').addClass('active');

	});

	$(document).on('click', '#shwcf form #create', function(e) {
		var form = $('#shwcf form').serializeArray();
		createWebsite(form);
	});

	$(document).on('click', '#shwcf form #fromScratch', function(e) {
		var form = $('#shwcf form').serializeArray();
		form[2].value = null;
		createWebsite(form);
	});

	function createWebsite(data) {
		toggleFormButtons(3);
		requestActive = true;
		$('#shwcf .content .step').removeClass('active');
		$('#s3').addClass('active');
		loader('Your website is being launched.', 'Please wait while our system is creating your website in the background.');
	}

	function loader(title, message){
		$('#s3').html('');
		var loaderHTML = `<div class="loader">
												<img src="${loaderFile}">
												<span class="title">${title}</span>
												<span>${message}</span>
											</div>`;
		$('#s3').html(loaderHTML);
		APIRequest('createWebsite','','');
	}

	function toggleFormButtons(tab){
		$('.actions').removeClass('active');
		$('.actions.step-' + tab).addClass('active');
	}

	function showAlertBox(type, title, message, links) {
		$('#s3').html('');
		var icon = assetPath + '/' + type + '.svg';
		var alertBox = `
		<div class="confirmation">
			<div class="alert-box">
					<span class="icon success" style="background: url('${icon}');"></span>
					<span class="title">${title}</span>
					<span class="message">${message}</span>
					<div class="actions">
						<button id="goToDashboard" type="button" class="btn sh-secondary-btn"> Account Dashbord</button>
						<button id="openWebsite" type="button" class="btn sh-primary-btn"> Open Website </button>
					</div>
			</div>
		</div>
		`;
		$('#s3').html(alertBox);
	}

	function APIRequest(endpoint, type, data) {
		$.ajax({
        url: sh_automation_api.ajax_url,
        type: 'POST',
        data: {
            'action': endpoint,
            'data': data
        },
        success:function(data) {
						const res = JSON.parse(data);
            showAlertBox('success', res.title, res.message, '');
        },
        error: function(errorThrown){
          	showAlertBox('error', 'Website Awesome Acme was created successfully!','You can now start managin your website. Contact support team to help you setting up your own website.', '');
        }
    });
	}


})( jQuery );
