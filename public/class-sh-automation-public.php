<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       Webforest Digital Solutions
 * @since      1.0.0
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/public
 * @author     Webforest Digital Solutions <info@webforest.solutions>
 */
class Sh_Automation_Public  {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sh_Automation_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sh_Automation_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		$randomizr = rand(100,999);

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sh-automation-public.css', array(), $randomizr, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sh_Automation_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sh_Automation_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$randomizr = rand(100,999);
		wp_enqueue_script( $this->plugin_name . '-fa', 'https://kit.fontawesome.com/189f4d5f2a.js', array( 'jquery' ), $randomizr, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sh-automation-public.js', array( 'jquery' ), $randomizr, true );
		wp_localize_script( $this->plugin_name, 'sh_automation_api' , array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}

	public function sh_website_creation_form( $atts ) {

		/**
		 * This will display the Website Creation Form
		 *
		 */

		 ob_start();
		 $args = array(
			 'themes' => $this->sanitizeThemes(get_option( $this->plugin_name )),
			 'loader' => get_option( $this->plugin_name )['loader']
		 );

		 include('partials/form/sh-website-creation.php');

		 return ob_get_clean();

	}

	public function sanitizeThemes( $options ) {

		$themes = array();

		if(isset( $options['theme_1'] ) && ! empty( $options['theme_1'] )) {
			array_push($themes, array($options['theme_1'], $options['theme_name_1'], $options['theme_url_1']));
		}

		if(isset( $options['theme_2'] ) && ! empty( $options['theme_2'] )) {
			array_push($themes, array($options['theme_2'], $options['theme_name_2'], $options['theme_url_2']));
		}

		if(isset( $options['theme_3'] ) && ! empty( $options['theme_3'] )) {
			array_push($themes, array($options['theme_3'], $options['theme_name_3'], $options['theme_url_3']));
		}

		if(isset( $options['theme_4'] ) && ! empty( $options['theme_4'] )) {
			array_push($themes, array($options['theme_4'], $options['theme_name_4'], $options['theme_url_4']));
		}

		if(isset( $options['theme_5'] ) && ! empty( $options['theme_5'] )) {
			array_push($themes, array($options['theme_5'], $options['theme_name_5'], $options['theme_url_5']));
		}

		return $themes;
	}

	/**
  * Ajax Request Website Creation
  *
  * @since      1.0.0
  */

	public function createWebsite() {

			$cloudways = new Sh_Automation_Cloudways_API('', '');

	    echo json_encode(array(
				'title' => $cloudways->createApp(),
				'message' => 'You can now start managin your website. Contact support team to help you setting up your own website.'
			));
	    exit();
	}

}
