<?php

/**
 * Fired during plugin activation
 *
 * @link       Webforest Digital Solutions
 * @since      1.0.0
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sh_Automation
 * @subpackage Sh_Automation/includes
 * @author     Webforest Digital Solutions <info@webforest.solutions>
 */
class Sh_Automation_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
