<?php

/**
 * The public-facing functionality of the plugin for Cloduways API Automation Request
 *
 * @link       Webforest Digital Solutions
 * @since      1.0.0
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/public/dependencies
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the API endpoints, functionalities and request handler.
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/public
 * @author     Webforest Digital Solutions <info@webforest.solutions>
 */


class Sh_Automation_Cloudways_API {

  protected $key;
  protected $email;

  public function __construct($key, $email)  {
    $this->key = $key;
    $this->email = $email;
  }

  public function createApp() {
    return 'HELLO WORLD!';
  }

}
