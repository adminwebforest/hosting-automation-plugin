<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       Webforest Digital Solutions
 * @since      1.0.0
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/admin
 * @author     Webforest Digital Solutions <info@webforest.solutions>
 */
class Sh_Automation_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sh_Automation_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sh_Automation_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sh-automation-admin.css', array(), rand(100,999), 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sh_Automation_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sh_Automation_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if ( ! did_action( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		}

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sh-automation-admin.js', array( 'jquery' ), rand(100,999), false );

	}

		/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

	    /**
	     * Add a settings page for this plugin to the Settings menu.
	     *
	     * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
	     *
	     *        Administration Menus: http://codex.wordpress.org/Administration_Menus
	     *
	     * add_options_page( $page_title, $menu_title, $capability, $menu_slug, $function);
	     *
	     * @link https://codex.wordpress.org/Function_Reference/add_options_page
	     */
	    add_menu_page( 'SH Automation', 'SH Automation', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page' ) ,'dashicons-image-filter', 56);

	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

	    /**
	     * Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
	     * The "plugins.php" must match with the previously added add_submenu_page first option.
	     */
	    $settings_link = array( '<a href="' . admin_url( 'plugins.php?page=' . $this->plugin_name ) . '">' . __( 'Settings', $this->plugin_name ) . '</a>', );

	    // -- OR --

	    // $settings_link = array( '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_name ) . '">' . __( 'Settings', $this->plugin_name ) . '</a>', );

	    return array_merge(  $settings_link, $links );

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_setup_page() {

	    include_once( 'partials/' . $this->plugin_name . '-admin-display.php' );

	}

	/**
	 * Validate fields from admin area plugin settings form ('exopite-lazy-load-xt-admin-display.php')
	 * @param  mixed $input as field form settings form
	 * @return mixed as validated fields
	 */
	public function validate($input) {

	    // $valid = array();

	    // $valid['example_checkbox'] = ( isset( $input['example_checkbox'] ) && ! empty( $input['example_checkbox'] ) ) ? 1 : 0;
	    // $valid['example_text'] = ( isset( $input['example_text'] ) && ! empty( $input['example_text'] ) ) ? esc_attr( $input['example_text'] ) : 'default';
	    // $example_textarea['example_textarea'] = ( isset( $input['example_textarea'] ) && ! empty( $input['example_textarea'] ) ) ? sanitize_textarea_field( $input['example_textarea'] ) : 'default';
	    // $valid['example_select'] = ( isset($input['example_select'] ) && ! empty( $input['example_select'] ) ) ? esc_attr($input['example_select']) : 1;

	    // return $valid;

	    // -- OR --

	    $options = get_option( $this->plugin_name );

	    $options['theme_1'] = ( isset( $input['theme_1'] ) && ! empty( $input['theme_1'] ) ) ? $input['theme_1'] : '';
			$options['theme_name_1'] = ( isset( $input['theme_name_1'] ) && ! empty( $input['theme_name_1'] ) ) ? $input['theme_name_1'] : '';
			$options['theme_url_1'] = ( isset( $input['theme_url_1'] ) && ! empty( $input['theme_url_1'] ) ) ? $input['theme_url_1'] : '';

			$options['theme_2'] = ( isset( $input['theme_2'] ) && ! empty( $input['theme_2'] ) ) ? $input['theme_2'] : '';
			$options['theme_name_2'] = ( isset( $input['theme_name_2'] ) && ! empty( $input['theme_name_2'] ) ) ? $input['theme_name_2'] : '';
			$options['theme_url_2'] = ( isset( $input['theme_url_2'] ) && ! empty( $input['theme_url_2'] ) ) ? $input['theme_url_2'] : '';

			$options['theme_3'] = ( isset( $input['theme_3'] ) && ! empty( $input['theme_3'] ) ) ? $input['theme_3'] : '';
			$options['theme_name_3'] = ( isset( $input['theme_name_3'] ) && ! empty( $input['theme_name_3'] ) ) ? $input['theme_name_3'] : '';
			$options['theme_url_3'] = ( isset( $input['theme_url_3'] ) && ! empty( $input['theme_url_3'] ) ) ? $input['theme_url_3'] : '';

			$options['theme_4'] = ( isset( $input['theme_4'] ) && ! empty( $input['theme_4'] ) ) ? $input['theme_4'] : '';
			$options['theme_name_4'] = ( isset( $input['theme_name_4'] ) && ! empty( $input['theme_name_4'] ) ) ? $input['theme_name_4'] : '';
			$options['theme_url_4'] = ( isset( $input['theme_url_4'] ) && ! empty( $input['theme_url_4'] ) ) ? $input['theme_url_4'] : '';

			$options['theme_5'] = ( isset( $input['theme_5'] ) && ! empty( $input['theme_5'] ) ) ? $input['theme_5'] : '';
			$options['theme_name_5'] = ( isset( $input['theme_name_5'] ) && ! empty( $input['theme_name_5'] ) ) ? $input['theme_name_5'] : '';
			$options['theme_url_5'] = ( isset( $input['theme_url_5'] ) && ! empty( $input['theme_url_5'] ) ) ? $input['theme_url_5'] : '';

			$options['loader'] = ( isset( $input['loader'] ) && ! empty( $input['loader'] ) ) ? $input['loader'] : '';
			$options['api_key'] = ( isset( $input['api_key'] ) && ! empty( $input['api_key'] ) ) ? $input['api_key'] : '';

	    return $options;

	}

	public function options_update() {

	    register_setting( $this->plugin_name, $this->plugin_name, array(
	       'sanitize_callback' => array( $this, 'validate' ),
	    ) );

	}

}
