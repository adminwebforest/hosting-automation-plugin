<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       Webforest Digital Solutions
 * @since      1.0.0
 *
 * @package    Sh_Automation
 * @subpackage Sh_Automation/admin/partials
 */

 if ( ! defined( 'WPINC' ) ) die;
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->


<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">
    <h2>Stack Host Automation</h2>

    <form method="post" class="sh-admin" name="<?php echo $this->plugin_name; ?>" action="options.php">
      <?php
      //Grab all options
      $options = get_option( $this->plugin_name );

      $theme_1 = ( isset( $options['theme_1'] ) && ! empty( $options['theme_1'] ) ) ? esc_attr( $options['theme_1'] ) : '';
      $theme_name_1 = ( isset( $options['theme_name_1'] ) && ! empty( $options['theme_name_1'] ) ) ? esc_attr( $options['theme_name_1'] ) : '';
      $theme_url_1 = ( isset( $options['theme_url_1'] ) && ! empty( $options['theme_url_1'] ) ) ? esc_attr( $options['theme_url_1'] ) : '';

      $theme_2 = ( isset( $options['theme_2'] ) && ! empty( $options['theme_2'] ) ) ? esc_attr( $options['theme_2'] ) : '';
      $theme_name_2 = ( isset( $options['theme_name_2'] ) && ! empty( $options['theme_name_2'] ) ) ? esc_attr( $options['theme_name_2'] ) : '';
      $theme_url_2 = ( isset( $options['theme_url_2'] ) && ! empty( $options['theme_url_2'] ) ) ? esc_attr( $options['theme_url_2'] ) : '';

      $theme_3 = ( isset( $options['theme_3'] ) && ! empty( $options['theme_3'] ) ) ? esc_attr( $options['theme_3'] ) : '';
      $theme_name_3 = ( isset( $options['theme_name_3'] ) && ! empty( $options['theme_name_3'] ) ) ? esc_attr( $options['theme_name_3'] ) : '';
      $theme_url_3 = ( isset( $options['theme_url_3'] ) && ! empty( $options['theme_url_3'] ) ) ? esc_attr( $options['theme_url_3'] ) : '';

      $theme_4 = ( isset( $options['theme_4'] ) && ! empty( $options['theme_4'] ) ) ? esc_attr( $options['theme_4'] ) : '';
      $theme_name_4 = ( isset( $options['theme_name_4'] ) && ! empty( $options['theme_name_4'] ) ) ? esc_attr( $options['theme_name_4'] ) : '';
      $theme_url_4 = ( isset( $options['theme_url_4'] ) && ! empty( $options['theme_url_4'] ) ) ? esc_attr( $options['theme_url_4'] ) : '';

      $theme_5 = ( isset( $options['theme_5'] ) && ! empty( $options['theme_5'] ) ) ? esc_attr( $options['theme_5'] ) : '';
      $theme_name_5 = ( isset( $options['theme_name_5'] ) && ! empty( $options['theme_name_5'] ) ) ? esc_attr( $options['theme_name_5'] ) : '';
      $theme_url_5 = ( isset( $options['theme_url_5'] ) && ! empty( $options['theme_url_5'] ) ) ? esc_attr( $options['theme_url_5'] ) : '';

      $loader = ( isset( $options['loader'] ) && ! empty( $options['loader'] ) ) ? esc_attr( $options['loader'] ) : '';

      $api_key = ( isset( $options['api_key'] ) && ! empty( $options['api_key'] ) ) ? esc_attr( $options['api_key'] ) : '';

      //var_dump($options);
      settings_fields($this->plugin_name);
      do_settings_sections($this->plugin_name);

      ?>

      <div class="form-group settings">

        <fieldset>
            <p><?php esc_attr_e( 'Cloudways API Key', 'plugin_name' ); ?></p>
            <input type="text" id="<?php echo $this->plugin_name; ?>-api_key" name="<?php echo $this->plugin_name; ?>[api_key]" value="<?php if( ! empty( $api_key ) ) echo $api_key; else echo ''; ?>"/>
        </fieldset>

        <fieldset>
            <p><?php esc_attr_e( 'Custom Loader', 'plugin_name' ); ?></p>
            <div class="preview-wrapper">
              <?php
              if( $loader_file = wp_get_attachment_image_src( $loader, 'full' ) ) {

                  echo '<a href="#" class="misha-upl"><img src="' . $loader_file[0] . '" /></a>
                        <a href="#" class="misha-rmv">Remove image</a>
                        ';

                } else {

                  echo '<a href="#" class="misha-upl">Upload image</a>
                        <a href="#" class="misha-rmv" style="display:none">Remove image</a>';

                }
                echo '<input type="hidden" name="'. $this->plugin_name .'[loader]" value="' . $loader_file . '">';
              ?>
            </div>
        </fieldset>

      </div>
      <div class="form-group repeater">
        <div class="header">
          <span class="preview">Preview</span>
          <span class="name">Theme Name</span>
          <span class="url">Preview URL</span>
        </div>

        <!-- THEME 1 -->
        <div class="row <?php if( ! empty( $theme_1 ) ) echo 'visible'; else echo ''; ?>">
          <fieldset class="preview">
              <?php
              if( $image = wp_get_attachment_image_src( $theme_1 ) ) {

                  echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
                        <a href="#" class="misha-rmv">Remove image</a>
                        ';

                } else {

                  echo '<a href="#" class="misha-upl">Upload image</a>
                        <a href="#" class="misha-rmv" style="display:none">Remove image</a>';

                }
                echo '<input type="hidden" name="'. $this->plugin_name .'[theme_1]" value="' . $theme_1 . '">';
              ?>
          </fieldset>
          <fieldset class="name">
              <input type="text" id="<?php echo $this->plugin_name; ?>-theme_name_1" name="<?php echo $this->plugin_name; ?>[theme_name_1]" value="<?php if( ! empty( $theme_name_1 ) ) echo $theme_name_1; else echo ''; ?>"/>
          </fieldset>
          <fieldset class="url">
              <input type="text" class="theme_url_1" id="<?php echo $this->plugin_name; ?>-theme_url_1" name="<?php echo $this->plugin_name; ?>[theme_url_1]" value="<?php if( ! empty( $theme_url_1 ) ) echo $theme_url_1; else echo ''; ?>"/>
          </fieldset>
        </div>

        <!-- THEME 2 -->
        <div class="row <?php if( ! empty( $theme_2 ) ) echo 'visible'; else echo ''; ?>">
          <fieldset class="preview">
              <?php
              if( $image = wp_get_attachment_image_src( $theme_2 ) ) {

                  echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
                        <a href="#" class="misha-rmv">Remove image</a>
                        ';

                } else {

                  echo '<a href="#" class="misha-upl">Upload image</a>
                        <a href="#" class="misha-rmv" style="display:none">Remove image</a>';

                }
                echo '<input type="hidden" name="'. $this->plugin_name .'[theme_2]" value="' . $theme_2 . '">';
              ?>
          </fieldset>
          <fieldset class="name">
              <input type="text" id="<?php echo $this->plugin_name; ?>-theme_name_2" name="<?php echo $this->plugin_name; ?>[theme_name_2]" value="<?php if( ! empty( $theme_name_2 ) ) echo $theme_name_2; else echo ''; ?>"/>
          </fieldset>
          <fieldset class="url">
              <input type="text" class="theme_url_2" id="<?php echo $this->plugin_name; ?>-theme_url_2" name="<?php echo $this->plugin_name; ?>[theme_url_2]" value="<?php if( ! empty( $theme_url_2 ) ) echo $theme_url_2; else echo ''; ?>"/>
          </fieldset>
        </div>

        <!-- THEME 3 -->
        <div class="row <?php if( ! empty( $theme_3 ) ) echo 'visible'; else echo ''; ?>">
          <fieldset class="preview">
              <?php
              if( $image = wp_get_attachment_image_src( $theme_3 ) ) {

                  echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
                        <a href="#" class="misha-rmv">Remove image</a>
                        ';

                } else {

                  echo '<a href="#" class="misha-upl">Upload image</a>
                        <a href="#" class="misha-rmv" style="display:none">Remove image</a>';

                }
                echo '<input type="hidden" name="'. $this->plugin_name .'[theme_3]" value="' . $theme_3 . '">';
              ?>
          </fieldset>
          <fieldset class="name">
              <input type="text" id="<?php echo $this->plugin_name; ?>-theme_name_3" name="<?php echo $this->plugin_name; ?>[theme_name_3]" value="<?php if( ! empty( $theme_name_3 ) ) echo $theme_name_3; else echo ''; ?>"/>
          </fieldset>
          <fieldset class="url">
              <input type="text" class="theme_url_3" id="<?php echo $this->plugin_name; ?>-theme_url_3" name="<?php echo $this->plugin_name; ?>[theme_url_3]" value="<?php if( ! empty( $theme_url_3 ) ) echo $theme_url_3; else echo ''; ?>"/>
          </fieldset>
        </div>

        <!-- THEME 4 -->
        <div class="row <?php if( ! empty( $theme_4 ) ) echo 'visible'; else echo ''; ?>">
          <fieldset class="preview">
              <?php
              if( $image = wp_get_attachment_image_src( $theme_4 ) ) {

                  echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
                        <a href="#" class="misha-rmv">Remove image</a>
                        ';

                } else {

                  echo '<a href="#" class="misha-upl">Upload image</a>
                        <a href="#" class="misha-rmv" style="display:none">Remove image</a>';

                }
                echo '<input type="hidden" name="'. $this->plugin_name .'[theme_4]" value="' . $theme_4 . '">';
              ?>
          </fieldset>
          <fieldset class="name">
              <input type="text" id="<?php echo $this->plugin_name; ?>-theme_name_4" name="<?php echo $this->plugin_name; ?>[theme_name_4]" value="<?php if( ! empty( $theme_name_4 ) ) echo $theme_name_4; else echo ''; ?>"/>
          </fieldset>
          <fieldset class="url">
              <input type="text" class="theme_url_4" id="<?php echo $this->plugin_name; ?>-theme_url_4" name="<?php echo $this->plugin_name; ?>[theme_url_4]" value="<?php if( ! empty( $theme_url_4 ) ) echo $theme_url_4; else echo ''; ?>"/>
          </fieldset>
        </div>

        <!-- THEME 5 -->
        <div class="row <?php if( ! empty( $theme_5 ) ) echo 'visible'; else echo ''; ?>">
          <fieldset class="preview">
              <?php
              if( $image = wp_get_attachment_image_src( $theme_5 ) ) {

                  echo '<a href="#" class="misha-upl"><img src="' . $image[0] . '" /></a>
                        <a href="#" class="misha-rmv">Remove image</a>
                        ';

                } else {

                  echo '<a href="#" class="misha-upl">Upload image</a>
                        <a href="#" class="misha-rmv" style="display:none">Remove image</a>';

                }
                echo '<input type="hidden" name="'. $this->plugin_name .'[theme_5]" value="' . $theme_5 . '">';
              ?>
          </fieldset>
          <fieldset class="name">
              <input type="text" id="<?php echo $this->plugin_name; ?>-theme_name_5" name="<?php echo $this->plugin_name; ?>[theme_name_5]" value="<?php if( ! empty( $theme_name_5 ) ) echo $theme_name_5; else echo ''; ?>"/>
          </fieldset>
          <fieldset class="url">
              <input type="text" class="theme_url_5" id="<?php echo $this->plugin_name; ?>-theme_url_5" name="<?php echo $this->plugin_name; ?>[theme_url_5]" value="<?php if( ! empty( $theme_url_5 ) ) echo $theme_url_5; else echo ''; ?>"/>
          </fieldset>
        </div>
      </div>
      <a class="add-theme">+ Add Theme</a>
    <?php submit_button( __( 'Save all changes', 'plugin_name' ), 'primary','submit', TRUE ); ?>
    </form>
</div>
