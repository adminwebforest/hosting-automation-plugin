<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              Webforest Digital Solutions
 * @since             1.0.0
 * @package           Sh_Automation
 *
 * @wordpress-plugin
 * Plugin Name:       SH Automation
 * Plugin URI:        www.getstackhost.com
 * Description:       This plugin integrates cloudways API to your Wordpress site.
 * Version:           1.0.0
 * Author:            Webforest Digital Solutions
 * Author URI:        Webforest Digital Solutions
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sh-automation
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SH_AUTOMATION_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sh-automation-activator.php
 */
function activate_sh_automation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sh-automation-activator.php';
	Sh_Automation_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sh-automation-deactivator.php
 */
function deactivate_sh_automation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sh-automation-deactivator.php';
	Sh_Automation_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sh_automation' );
register_deactivation_hook( __FILE__, 'deactivate_sh_automation' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sh-automation.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sh_automation() {

	$plugin = new Sh_Automation();
	$plugin->run();

}
run_sh_automation();
